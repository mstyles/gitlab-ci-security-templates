# Gitlab CI Security Templates

This is a repository of yaml files to be used as includes within a repository's `.gitlab-ci.yml` file.  Each includable yaml file represents a security tool for a specific language or platform, e.g. `npm audit`, which uses `audit-ci` to find vulnerable packages for NodeJS applications.  Please see the primary documentation below for more information and usage instructions.

## Primary Documentation

[https://www.mediawiki.org/wiki/Security/Application_Security_Pipeline](https://www.mediawiki.org/wiki/Security/Application_Security_Pipeline)

## Dependencies

As these are just yaml files, there are no dependencies in a traditional sense.  There are certain _implied_ dependencies, however, including:

* A version of Gitlab with its native CI/CD functionality enabled.
* Various container images hosted under [docker-registry.wikimedia.org](https://docker-registry.wikimedia.org).
* The various security tools (e.g. `audit-ci`) as installed and run via the yaml files. 

## Contributing

We openly welcome contributions to this project, from bug reports to new feature development!  Please reach out to us at [security-help@wikimedia.org](mailto:security-help@wikimedia.org) if you would like to get involved or feel free to view our current project hierarchy within Wikimedia's Phabricator bug tracker:

[https://phabricator.wikimedia.org/T289290](https://phabricator.wikimedia.org/T289290)

(TODO: create a proper project tag)

## Authors

* [Manfredi Martorana](mailto:mmartorana@wikimedia.org)
* [Maryum Styles](mailto:mstyles@wikimedia.org)
* [Scott Bassett](mailto:sbassett@wikimedia.org)

## Version History

* 0.1
    * Initial beta release

## License

This project is licensed under the Apache 2.0 License - see the [LICENSE](LICENSE) file for details.

## References

* [project documentation](https://www.mediawiki.org/wiki/Security/Application_Security_Pipeline)
* [gitlab ci/cd](https://docs.gitlab.com/ee/ci/) 
* [gitlab ci include files](https://docs.gitlab.com/ee/ci/yaml/includes.html)
